<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="com.assignment.books.dbresource.DbConnection"%>
<%@page import="com.assignment.books.dao.BookDao"%>
<%@page import="com.assignment.books.bean.*"%>
<%@page import="java.util.*"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>


<!DOCTYPE html>

<html>

<head>


<meta charset="ISO-8859-1">

<title>Home Page</title>

 <link href="https://fonts.googleapis.com/css?family=ZCOOL+XiaoWei" rel="stylesheet">
 <link href="css/style.css" rel="stylesheet" type="text/css"/>

</head>

<body style="text-align:center;">
<h1>BookStore</h1>
<br>
<a href="login.jsp"><button>Login</button></a></br>
<a href="registration.jsp"><button>Sign in</button></a>
<br>
<br>
	<div class="container">
		<div class="card-header my-3">All Books</div>
		<div class="row">
		
		<style>
		.bhavana{
		float:left;
		background-color: lightgrey;
		height:450px;
  		width: 200px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.divya{
		float:left;
		background-color: lightgrey;
		height:450px;
  		width: 200px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.indu{
		float:left;
		background-color: lightgrey;
		height:450px;
  		width: 200px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.sevi{
		float:left;
		background-color: lightgrey;
		height:450px;
  		width: 200px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.yashu{
		float:left;
		background-color: lightgrey;
		height:450px;
  		width: 200px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.charan{
		float:left;
		background-color: lightgrey;
		height:450px;
  		width: 200px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
</style>
		
		<div class="bhavana">
		<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/richdadpoordad.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:1</h5>
						<h5 class="booktitle" >BookTitle:"Rich Dad Poor Dad"</h5>
						<h6 class="bookgenre">Bookgenre:"Motivational"</h6>
						
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark"><button>Add to Cart</button></a> 
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="divya">
		
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/clarissa.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:2</h5>
						<h5 class="booktitle" >BookTitle:"Clarissa"</h5>
						<h6 class="bookgenre">Bookgenre:"Romantic"</h6>
						
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark"><button>Add to Cart</button></a> 
						</div>
					</div>
				</div>	
			</div>	
		</div>	
			
			<div class="sevi">
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/aliceinwonderland.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:3</h5>
						<h5 class="booktitle" >BookTitle:"Alice in Wonderland"</h5>
						<h6 class="bookgenre">Bookgenre:"cartoon"</h6>
						
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark"><button>Add to Cart</button></a> 
						</div>
					</div>
				</div>
			</div>
		</div>
			<div class="yashu">
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/sybil.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:4</h5>
						<h5 class="booktitle" >BookTitle:"Sybil"</h5>
						<h6 class="bookgenre">Bookgenre:"Horror"</h6>
						
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark"><button>Add to Cart</button></a> 
						</div>
					</div>
				</div>
			</div>
		</div>
			<div class="charan">
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/littlewomen.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:5</h5>
						<h5 class="booktitle" >BookTitle:" Little Women"</h5>
						<h6 class="bookgenre">Bookgenre:"Lady Oriented""</h6>
						
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark" href="add-to-favourite?id=2"><button>Add to Cart</button></a> 
						</div>
					</div>
				</div>
			</div>
		</div>
			<div class="Shine">
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/frankenstein.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:6</h5>
						<h5 class="booktitle" >BookTitle:"Frankenstein"</h5>
						<h6 class="bookgenre">Bookgenre:"Horror"</h6>
						
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark" href="add-to-favourite?id=2"><button>Add to Cart</button></a> 
						</div>
					</div>
				</div>
			</div>
		</div>
			
		</div>
	</div>

</body>
</html>