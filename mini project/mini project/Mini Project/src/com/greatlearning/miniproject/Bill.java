package com.greatlearning.miniproject;
import java.time.LocalDateTime;
import java.util.*;

public class Bill {
	private String name;
	private List<Menu> items;
	private double cost;
	private Date time;
	
	public Bill() { }
    
	public Bill(String name, List<Menu> items, double cost, Date time) throws IllegalArgumentException {
		super();
		
		this.name = name;
		this.items = items;
		if(cost<0)
		{
			throw new IllegalArgumentException("exception occured, cost cannot less than zero");
		}
		this.cost = cost;
		this.time = time;
		
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public List<Menu> getMenu() {
		return items;
	}
	public void setMenu(List<Menu> selectedMenu) {
		this.items = selectedMenu;
	}
	
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	
	public Date getTime() {
		return time;
	}
	public void setTime(Date date) {
		this.time = date;
	}
		
}


