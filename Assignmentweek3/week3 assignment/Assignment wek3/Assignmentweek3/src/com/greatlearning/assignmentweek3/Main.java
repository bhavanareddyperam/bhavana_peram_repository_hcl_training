package com.greatlearning.assignmentweek3;
import java.util.*;

public class Main {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner=new Scanner(System.in);
		   MagicOfBooks mb=new MagicOfBooks();
		   
			while (true) {
				System.out.println("1. add entry into book\r\n"
						+ "2.delete entries from book\r\n" + "3.update a book\r\n"
						+ "4. display all the books \r\n" + "5. total count of all the books\r\n"
						+ "6.search for autobiography books" +  "\n7.display by features");
	       
				System.out.println("enter your choice:");
				int choice = scanner.nextInt();

				switch (choice) {

				case 1://Adding a book
					System.out.println("Please Enter Number of Books you want to add");
					int n=scanner.nextInt();
					
					for(int i=1;i<=n;i++) {
						mb.addbook();
						}
					break;
				
				case 2://Deleting a book
					   mb.deletebook();
					    break;
		
				case 3://Updating a book
					mb.updatebook();
					break;

				case 4:
					mb.displayBookInfo();
					break;
				
				case 5:
					System.out.println("Count of all books-");
					mb.count();
					break;
					
				case 6:
					mb.autobiography();
				      break;
				      
				
				case 7:
					System.out.println("Enter your choice:\n 1. Price low to high "
							+ "\n 2.Price high to low \n 3. Best selling");
					int ch = scanner.nextInt();

					switch (ch) {

					case 1 : mb.displayByFeature(1);
					         break;
					case 2:mb.displayByFeature(2); 
					         break;
					case 3:mb.displayByFeature(3);
					        break;
					}
				
				default:
					System.out.println("entered wrong choice.!");

				}

			}

	      }



	}
