
package com.greatlearning.assignment;

public class TechDept extends SuperDepartment
{
	public String departmentName()
	{
		return "TECH DEPT";
	}
	public String getTodaysWork()
	{
		return "COMPLETE CODING OF MODULE 1";
	} 
	public String getWorkDeadline()
	{
		return "COMPLETE BY EOD";
	}
	public String getStackInfo()
	{
		return "CORE JAVA";
	}

}
