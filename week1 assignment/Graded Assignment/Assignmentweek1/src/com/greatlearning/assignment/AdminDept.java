package com.greatlearning.assignment;

public class AdminDept extends SuperDepartment
{
	public String departmentName()
	{
		return "ADMIN DEPT";
	}
	public String getTodayWork()
	{
		return "COMPLETE YOUR DOCUMENTS SUBMISSION";
	} 
	public String getWorkDeadline()
	{
		return "COMPLETE BY EOD";
	}

}
