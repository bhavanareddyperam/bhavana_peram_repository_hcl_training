package com.spring.model;

public class ReadLater {

	private int readID;
	private String user_name;
	private int book_id;
	private String title;
	private int total_pages;
	private float rating;
	private int isbn;

	public int getReadID() {
		return readID;
	}

	public void setReadID(int readID) {
		this.readID = readID;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public int getBook_id() {
		return book_id;
	}

	public void setBook_id(int book_id) {
		this.book_id = book_id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getTotal_pages() {
		return total_pages;
	}

	public void setTotal_pages(int total_pages) {
		this.total_pages = total_pages;
	}

	public float getRating() {
		return rating;
	}

	public void setRating(float rating) {
		this.rating = rating;
	}

	public int getIsbn() {
		return isbn;
	}

	public void setIsbn(int isbn) {
		this.isbn = isbn;
	}

	public ReadLater(String user_name, int book_id, String title, int total_pages, float rating, int isbn) {
		super();
		this.user_name = user_name;
		this.book_id = book_id;
		this.title = title;
		this.total_pages = total_pages;
		this.rating = rating;
		this.isbn = isbn;
	}

	public ReadLater() {
		super();
		// TODO Auto-generated constructor stub
	}

}