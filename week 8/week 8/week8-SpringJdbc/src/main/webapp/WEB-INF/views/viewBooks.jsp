<%@page import="com.spring.model.Book"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<%@page import="java.util.List"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Display Books List</title>
<style>
a{
  font-size: 30px;
  color:#FF00FF;
}
</style>
</head>
<body>

		<h1 style="font-size: 80px;color:red;text-align:center;">Welcome To Book Store</h1>
		<p>
			<a href="login">Login</a>
		</p>
		<p>
			<a href="registration">Create Account (or) Register</a>
		</p>
		<p>
			<a href="index">Back To Index</a>
		</p>
		<h2 style="font-size:40px;color:#00FF00;text-align:center;">Displaying Books WithOut Login</h2>
  <table border="1" align="center" style="background-color:#52ffff;font-size:25px;border:2px solid Violet">
			<tr>
				<td>Book_ID</td>
				<td >Title</td>
				<td >Total Pages</td>
				<td>Rating</td>
				<td>Isbn</td>
			</tr>
			<%
			@SuppressWarnings("unchecked")
			List<Book> book = (List<Book>) request.getAttribute("list");
			%>
			<%
			for (Book b : book) {
			%>
			<tr>
				<td><%=b.getBook_id()%></td>
				<td><%=b.getTitle()%></td>
				<td><%=b.getTotal_pages()%></td>
				<td><%=b.getRating()%></td>
				<td><%=b.getIsbn()%> </td>
			</tr>
			<%
			}
			%>
		</table>
</body>
</html>