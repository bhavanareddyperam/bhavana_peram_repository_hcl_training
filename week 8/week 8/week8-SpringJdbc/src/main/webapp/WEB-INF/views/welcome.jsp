
<%@page import="com.spring.model.Book"%>

<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Welcome Page</title>


</head>
<body>
		<%
		String username = (String) request.getAttribute("username");
		session.setAttribute("username", username);
		%>
		<h1 style="font-size: 60px;text-align: center; color:blue; ">Welcome To Book Store</h1>
		<h2 style="color:orange;font-size: 30px;">Hi..  <%=username%> ..... Welcome to Book Store</h2>
		<form method="post" action="saveBooks">
		<table border="1" align="center" style="background-color:#52ffff;font-size:20px;border:2px solid Violet">
					<tr>
						<td style="color:pink;">Book_ID</td>
						<td style="color:pink;">Book_Title</td>
						<td style="color:pink;">Book_Total_pages</td>
						<td style="color:pink;">Book_Rating</td>
						<td style="color:pink;">Book_Isbn</td>
						<td style="color:pink;">ReadLater_Books</td>
						<td style="color:pink;">Liked_Books</td>
					</tr>
					<%
					@SuppressWarnings("unchecked")
					List<Book> book = (List<Book>) request.getAttribute("list");
					%>
					<%
					for (Book b : book) {
					%>
					<tr>
						<td><%=b.getBook_id()%></td>
				        <td><%=b.getTitle()%></td>
				        <td><%=b.getTotal_pages()%></td>
				        <td><%=b.getRating()%></td>
				        <td><%=b.getIsbn()%> </td>
						<td><a href="#"><button	type="button">Read Later</button></a></td>
						<td><a href="#"><button type="button">Like</button></a></td>
					</tr>
					<%
					}
					%>
				</table>
			</form>
	<p>
		<a href="displayReadLaterBooks" style="font-size: 30px;color:Green;">Read Later Books</a>
	</p>
	<p>
			<a href="displayLikedBooks" style="font-size:30px;color:Green">Liked Books</a>
	</p>	
		
</body>
</html>