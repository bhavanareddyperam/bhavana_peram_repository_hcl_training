<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login Here</title>

</head>
<body>
	<h1 style="font-size:60px;color:green;">Login Account</h1>
 <form:form id="loginForm" modelAttribute="login" action="loginProcess" method="post">
 <div>
               <table>
                    <tr>
                        <td style="font-size:30px;color:orange;">
           <form:label path="username">UserName: </form:label>
                        </td>
                        <td>
                            <form:input path="username" name="username" id="username" />
                        </td>
                    </tr>
                    <tr >
                        <td style="font-size:30px;color:orange;">
                            <form:label path="password">Password:</form:label>
                        </td>
                        <td>
                            <form:password path="password" name="password" id="password" />
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td align="left">
                            <form:button id="login" name="login">Login</form:button>
                        </td>
                    </tr>
            </form:form>
            </table>
             
			<p>
             	<a href="registration" style="font-size:30px;">Create New Account</a>
             </p>
             <p>
           		<a href="index" style="font-size:30px;">Back To Index</a>
           	 </p>
         
        </div>
        </body>
        </html>
