create database  mvc;
use mvc;


create table books(
  book_id        int not null, 
  title          varchar(255) not null, 
  total_pages    int not null, 
  rating         decimal(4, 2)not null, 
  isbn           int not null, 
  primary key(book_id)
);


insert into books values(1,"Harry Potter",250,4.5,145415),
(2,"The Great Gatsby",450,4.5,452451),
(3,"GoodNight Moon",500,3.5,145451),
(4,"The Famous Five",800,4.6,142451),
(5,"The Short Stories",600,4.1,152451),
(6,"The Jungle Book",150,4.2,222451);
select * from books;

create table users (
   user_id 	   int not null auto_increment,
   username        varchar(45) not null,
   email 	   varchar(45) not null,
   password    varchar(45) not null,
   primary key(user_id)
);
insert into users values(1,"bhavana","bhavanareddyperam@gmail.com","1234");

select * from users;
