package com.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Books {
	@Id	
  private int id;
  private String title;
  private String author;
  private float price;
  
public Books() {
	super();
	// TODO Auto-generated constructor stub
}

public Books(int id, String title, String author, float price) {
	super();
	this.id = id;
	this.title = title;
	this.author = author;
	this.price = price;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getTitle() {
	return title;
}
public void setTitle(String title) {
	this.title = title;
}
public String getAuthor() {
	return author;
}
public void setAuthor(String author) {
	this.author = author;
}
public float getPrice() {
	return price;
}
public void setPrice(float price) {
	this.price = price;
}
@Override
public String toString() {
	return "Books [id=" + id + ", title=" + title + ", author=" + author + ", price=" + price + "]";
}
  
}
