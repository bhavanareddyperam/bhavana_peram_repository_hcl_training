package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bean.Books;
import com.dao.BooksDao;

@Service
public class BooksService {
	@Autowired
	BooksDao booksDao;
	
	//Store The Book Details
	public String storeBookDetails(Books book) {
		if(!booksDao.existsById(book.getId())) {
			booksDao.save(book);
			return "book Details stored sucessfully";
		}else {
			return "book Details already present";
		}
	}
	
	//Update Book Price By ID
	public String updateBookDetails(Books book) {
		if(booksDao.existsById(book.getId())) {
			Books bb=booksDao.getById(book.getId());
			if(bb.getPrice()==(book.getPrice())) {
				return "You are providing the old input";
			}else {
				bb.setPrice(book.getPrice());
				booksDao.saveAndFlush(bb);
				return "book details are updated sucessfully";
			}
		}else {
			return "book details are not present";			
		}						
	}
	
	public List<Books> getAllBooksAvaliable(){
		return booksDao.findAll();
	}
	
	//Get Book Details By Giving Id
	public Books findBooksById(int id) {
		if(booksDao.existsById(id)) {
			return booksDao.findById(id).get();
		}else {
			return null;
		}
		
	}
	
	//Deleted Book By ID
	public String deleteBookById(int id) {
		if(!booksDao.existsById(id)) {
			return "Book Details Not Present";
		}else {
			booksDao.deleteById(id);
			return "Book Deleted Sucessfully";
		}
	}
	
	
}
