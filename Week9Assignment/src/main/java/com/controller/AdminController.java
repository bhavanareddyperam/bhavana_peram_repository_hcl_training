package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bean.Admin;
import com.bean.Books;
import com.service.AdminService;
@RestController
@RequestMapping(value="admin")
public class AdminController {
	@Autowired
	AdminService adminService;
	
	@PostMapping(value="register",consumes=MediaType.APPLICATION_JSON_VALUE)
	public String adminRegister(@RequestBody Admin adm) {
		return adminService.adminRegistration(adm);
	}
	
	@PatchMapping(value="checkAdminDetails")
	public String checkAdminInfo(@RequestBody Admin adm) {
		return adminService.checkAdminDetails(adm);
		
	}
	
	@GetMapping(value = "logout")
	public String adminLogout() {
		return "You logged out Sucessfully";
	}

	@GetMapping(value="Details")
	public List<Admin> getAllAdminAvaliable() {
		return adminService.getAllAdminAvaliable();
	}
}
